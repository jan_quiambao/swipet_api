# README #

### What is this repository for? ###

* API of SwiPet Mobile Application
* config/ will server as the main project
* Additional apps can be installed separately by modifying the settings.py file in config/

### How do I get set up? ###

* Copy .env.dev into config/ and rename it to .env
* Edit the contents of .env as needed
* Install libraries used in requirements.txt
* Run: "python3 manage.py migrate" before starting the development server

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* For concerns, please send an email to jan@directworksmedia.com or reyan@directworksmedia.com