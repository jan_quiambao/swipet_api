
from django.contrib import admin
from django.urls    import include, path, re_path

from drf_yasg               import openapi
from drf_yasg.views         import get_schema_view
from account import views as account_view
from rest_framework import permissions
from rest_framework.routers import DefaultRouter

schema_view = get_schema_view(
    openapi.Info(
        title='SwiPet API',
        default_version='v0.0.1',
        description='Rest API for the SwiPet Application',
        contact=openapi.Contact(email='jan@directworksmedia.com'),
    ),
    public=True,
    permission_classes=[permissions.AllowAny,],
)


def router():
    router = DefaultRouter()
    router.register('account', account_view.Account, basename='account')
    return router


urlpatterns = [
    path('o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    re_path(r'^api/docs/(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    re_path(r'^api/docs/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('admin/', admin.site.urls),
    path('api/petfinder/', include('petfinder.urls')),
    path('v1/', include(router().urls))
]


