from datetime import timedelta

from django.utils.timezone import now
from oauth2_provider.models import AccessToken, Application, RefreshToken
from oauthlib.oauth2.rfc6749.tokens import random_token_generator
from rest_framework.status import HTTP_404_NOT_FOUND

from config.settings import OAUTH2_PROVIDER


class AccessTokenGenerator(object):

    expires_after = OAUTH2_PROVIDER["ACCESS_TOKEN_EXPIRE_SECONDS"]
    scope = OAUTH2_PROVIDER["SCOPES"]

    @classmethod
    def generate(cls, request, user, client_id, client_secret):
        application = Application.objects.filter(client_id=client_id, client_secret=client_secret).first()
        access_token = AccessToken.objects.filter(user__pk=user.pk).order_by("-pk").first()
        expires = now() + timedelta(seconds=cls.expires_after)

        if not application:
            return ValueError("Invalid client credentials.")

        if not access_token or not access_token.is_valid():
            access_token = AccessToken.objects.create(
                user=user,
                token=random_token_generator(request),
                application=application,
                expires=expires,
                scope=cls.scope
            )

        refresh_token, is_created = RefreshToken.objects.get_or_create(
            user=user,
            access_token=access_token,
            application=application
        )

        if is_created:
            refresh_token.token = random_token_generator(request)
            refresh_token.save()

        return refresh_token

    @classmethod
    def refresh_token(cls, request, token, client_id, client_secret):
        application = Application.objects.filter(client_id=client_id, client_secret=client_secret).first()
        refresh_token = RefreshToken.objects.filter(token=token, application=application).first()

        if not refresh_token or not application:
            return HTTP_404_NOT_FOUND

        user = refresh_token.user
        application = refresh_token.application
        scope = cls.scope
        expires = now() + timedelta(seconds=cls.expires_after)

        access_token = AccessToken.objects.create(
            user=user,
            application=application,
            expires=expires,
            scope=scope,
            token=random_token_generator(request)
        )

        refresh_token.access_token = access_token
        refresh_token.save()

        return access_token

    @staticmethod
    def get_application():
        return Application.objects.all().first()
