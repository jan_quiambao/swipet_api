from rest_framework import serializers


class ErrorDetailSerializer(serializers.Serializer):
    error = serializers.CharField(max_length=255)


class GenericDetailSerializer(serializers.Serializer):
    detail = serializers.CharField(max_length=255)
