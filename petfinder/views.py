import requests

from django.conf import settings

from drf_yasg.utils import swagger_auto_schema

from rest_framework             import status
from rest_framework.decorators  import api_view
from rest_framework.response    import Response

from common.serializers import ErrorDetailSerializer

from petfinder.serializers import (
    AnimalCompleteSerializer,
    AnimalListSerializer,
    AnimalTypeSerializer,
    AnimalBreedsSerializer,
    OrganizationSerializer,
    AnimalQuerySerializer,
)


# Create your views here.
def GetPetFinderAuthorizationToken(request):
    token = request.session.get('access_token')
    if token is not None:
        return token

    petfinder_url = 'https://api.petfinder.com/v2/oauth2/token'
    data = {
        'grant_type': 'client_credentials',
        'client_id': settings.PETFINDER_CLIENT_ID,
        'client_secret': settings.PETFINDER_CLIENT_SECRET,
    }

    petfinder_response = requests.post(petfinder_url, data=data)
    petfinder_data = petfinder_response.json()

    if petfinder_response.status_code == 200:
        request.session['petfinder_session'] = petfinder_data
        access_token = request.session.get('petfinder_session').get('access_token')

    else:
        access_token = None

    return access_token


def GetHeaders(request):
    return {'Authorization': 'Bearer {token}'.format(token=GetPetFinderAuthorizationToken(request)),}


@swagger_auto_schema(
    method='GET',
    query_serializer=AnimalQuerySerializer,
    responses={
        200: AnimalListSerializer,
        400: ErrorDetailSerializer,
        401: ErrorDetailSerializer,
        403: ErrorDetailSerializer,
        500: ErrorDetailSerializer,
    },
)
@api_view(['GET'])
def get_animals(request):
    """
    API Endpoint to retrieve the information of 50 animals.
    To retrieve a specific type of animal, simply fill in the "type" parameter.
    To get a different page of animals, simply fill in the "pages" parameter.
    """
    headers = GetHeaders(request)

    type = request.GET.get('type')
    page = request.GET.get('page')
    location = request.GET.get('location')
    gender = request.GET.get('gender')
    age = request.GET.get('age')
    breed = request.GET.get('breed')

    base_petfinder_url = 'https://api.petfinder.com/v2/animals?limit=50'

    petfinder_url_components = []
    petfinder_url_components.append(base_petfinder_url)

    if type is not None:
        petfinder_url_components.append('&type={type}'.format(type=type))

    if page is not None:
        petfinder_url_components.append('&page={page}'.format(page=page))

    if location is not None:
        petfinder_url_components.apped('&location={location}'.format(location=location))

    if gender is not None:
        petfinder_url_components.append('&gender={gender}'.format(gender=gender))

    if age is not None:
        petfinder_url_components.append('&age={age}'.format(age=age))

    if breed is not None:
        petfinder_url_components.append('&breed={breed}'.format(breed=breed))
    
    petfinder_url = ''.join(petfinder_url_components)
    petfinder_response = requests.get(petfinder_url, headers=headers)
    petfinder_data = petfinder_response.json()

    if petfinder_response.status_code == 200:
        return Response(AnimalListSerializer(petfinder_data).data, status=status.HTTP_200_OK)

    else:
        error_title = petfinder_data.get('title')
        error_detail = petfinder_data.get('detail')
        return Response(
            ErrorDetailSerializer(
                {'error': '{title} {detail}'.format(title=error_title, detail=error_detail)}
            ).data,
            status=petfinder_response.status_code,
        )


@swagger_auto_schema(
    method='GET',
    responses={
        200: AnimalCompleteSerializer,
        400: ErrorDetailSerializer,
        401: ErrorDetailSerializer,
        403: ErrorDetailSerializer,
        500: ErrorDetailSerializer,
    },
)
@api_view(['GET'])
def get_animal(request, id):
    """
    API Endpoint to get a specific animal using it's id.
    """
    headers = GetHeaders(request)

    petfinder_url = 'https://api.petfinder.com/v2/animals/{id}'.format(id=id)
    petfinder_response = requests.get(petfinder_url, headers=headers)
    petfinder_data = petfinder_response.json()

    if petfinder_response.status_code == 200:
        animal = petfinder_data.get('animal')
        return Response(AnimalCompleteSerializer(animal).data, status=status.HTTP_200_OK)

    else:
        error_title = petfinder_data.get('title')
        error_detail = petfinder_data.get('detail')
        return Response(
            ErrorDetailSerializer(
                {'error': '{title} {detail}'.format(title=error_title, detail=error_detail)}
            ).data,
            status=petfinder_response.status_code,
        )


@swagger_auto_schema(
    method='GET',
    responses={
        200: AnimalTypeSerializer,
        400: ErrorDetailSerializer,
        401: ErrorDetailSerializer,
        403: ErrorDetailSerializer,
        500: ErrorDetailSerializer,
    },
)
@api_view(['GET'])
def get_animals_types(request):
    """
    API Endpoint to fetch all available animal types.
    """
    headers = GetHeaders(request)

    petfinder_url = 'https://api.petfinder.com/v2/types'
    petfinder_response = requests.get(petfinder_url, headers=headers)
    petfinder_data = petfinder_response.json()

    if petfinder_response.status_code == 200:
        animal_types = petfinder_data.get('types')
        return Response(AnimalTypeSerializer(animal_types, many=True).data, status=status.HTTP_200_OK)

    else:
        error_title = petfinder_data.get('title')
        error_detail = petfinder_data.get('detail')
        return Response(
            ErrorDetailSerializer(
                {'error': '{title} {detail}'.format(title=error_title, detail=error_detail)}
            ).data,
            status=petfinder_response.status_code,
        )


@swagger_auto_schema(
    method='GET',
    responses={
        200: AnimalTypeSerializer,
        400: ErrorDetailSerializer,
        401: ErrorDetailSerializer,
        403: ErrorDetailSerializer,
        500: ErrorDetailSerializer,
    },
)
@api_view(['GET'])
def get_animal_type(request, type):
    """
    API Endpoint to get a specific animal type.
    """
    headers = GetHeaders(request)

    petfinder_url = 'https://api.petfinder.com/v2/types/{type}'.format(type=type)
    petfinder_response = requests.get(petfinder_url, headers=headers)
    petfinder_data = petfinder_response.json()

    if petfinder_response.status_code == 200:
        animal_type = petfinder_data.get('type')
        return Response(AnimalTypeSerializer(animal_type).data, status=status.HTTP_200_OK)

    else:
        error_title = petfinder_data.get('title')
        error_detail = petfinder_data.get('detail')
        return Response(
            ErrorDetailSerializer(
                {'error': '{title} {detail}'.format(title=error_title, detail=error_detail)}
            ).data,
            status=petfinder_response.status_code,
        )


@swagger_auto_schema(
    method='GET',
    responses={
        200: AnimalBreedsSerializer,
        400: ErrorDetailSerializer,
        401: ErrorDetailSerializer,
        403: ErrorDetailSerializer,
        500: ErrorDetailSerializer,
    },
)
@api_view(['GET'])
def get_animal_breeds(request, type):
    """
    API Endpoint to get all breeds of a specifieid type.
    """
    headers = GetHeaders(request)

    petfinder_url = 'https://api.petfinder.com/v2/types/{type}/breeds'.format(type=type)
    petfinder_response = requests.get(petfinder_url, headers=headers)
    petfinder_data = petfinder_response.json()

    if petfinder_response.status_code == 200:
        animal_breeds = petfinder_data.get('breeds')
        return Response(AnimalBreedsSerializer(animal_breeds, many=True).data, status=status.HTTP_200_OK)

    else:
        error_title = petfinder_data.get('title')
        error_detail = petfinder_data.get('detail')
        return Response(
            ErrorDetailSerializer(
                {'error': '{title} {detail}'.format(title=error_title, detail=error_detail)}
            ).data,
            status=petfinder_response.status_code,
        )


@swagger_auto_schema(
    method='GET',
    responses={
        200: OrganizationSerializer,
        400: ErrorDetailSerializer,
        401: ErrorDetailSerializer,
        403: ErrorDetailSerializer,
        500: ErrorDetailSerializer,
    },
)
@api_view(['GET'])
def get_organization(request, id):
    """
    API Endpoint to fetch the organization where the pet belongs to.
    """
    headers = GetHeaders(request)

    petfinder_url = 'https://api.petfinder.com/v2/organizations/{id}'.format(id=id)
    petfinder_response = requests.get(petfinder_url, headers=headers)
    petfinder_data = petfinder_response.json()

    if petfinder_response.status_code == 200:
        organization = petfinder_data.get('organization')
        return Response(OrganizationSerializer(organization).data, status=status.HTTP_200_OK)

    else:
        error_title = petfinder_data.get('title')
        error_detail = petfinder_data.get('detail')
        return Response(
            ErrorDetailSerializer(
                {'error': '{title} {detail}'.format(title=error_title, detail=error_detail)}
            ).data,
            status=petfinder_response.status_code,
        )


