from django.urls import path

from petfinder import views

app_name = 'petfinder'

urlpatterns = [
    path('animal/<int:id>/', views.get_animal, name='animal'),
    path('animals/', views.get_animals, name='animals'),
    path('animals/types/', views.get_animals_types, name='animals-types'),
    path('animals/types/<str:type>/', views.get_animal_type, name='animals-type'),
    path('animals/types/<str:type>/breeds/', views.get_animal_breeds, name='animals-breeds'),
    path('organization/<str:id>/', views.get_organization, name='organization'),
]