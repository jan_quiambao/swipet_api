from rest_framework import serializers


# Component Serializers
class BreedSerializer(serializers.Serializer):
    primary = serializers.CharField()
    secondary = serializers.CharField()
    mixed = serializers.BooleanField()
    unknown = serializers.BooleanField()


class ColorSerializer(serializers.Serializer):
    primary = serializers.CharField()
    secondary = serializers.CharField()
    tertiary = serializers.CharField()


class AttributeSerializer(serializers.Serializer):
    spayed_neutered = serializers.BooleanField()
    house_trained = serializers.BooleanField()
    declawed = serializers.BooleanField()
    special_needs = serializers.BooleanField()
    shots_current = serializers.BooleanField()


class EnvironmentSerializer(serializers.Serializer):
    children = serializers.BooleanField()
    dogs = serializers.BooleanField()
    cats = serializers.BooleanField()


class PhotosSerializer(serializers.Serializer):
    small = serializers.URLField()
    medium = serializers.URLField()
    large = serializers.URLField()
    full = serializers.URLField()


class VideosSerializer(serializers.Serializer):
    embed = serializers.CharField()


class AddressSerializer(serializers.Serializer):
    address1 = serializers.CharField()
    address2 = serializers.CharField()
    city = serializers.CharField()
    state = serializers.CharField()
    postcode = serializers.CharField()
    country = serializers.CharField()


class ContactSerializer(serializers.Serializer):
    email = serializers.EmailField()
    phone = serializers.CharField()
    address = AddressSerializer()


class PaginationSerializer(serializers.Serializer):
    count_per_page = serializers.IntegerField()
    total_count = serializers.IntegerField()
    current_page = serializers.IntegerField()
    total_pages = serializers.IntegerField()


class AnimalSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    url = serializers.URLField()
    type = serializers.CharField()
    species = serializers.CharField()
    breeds = BreedSerializer()
    age = serializers.CharField()
    gender = serializers.CharField()
    name = serializers.CharField()
    photos = serializers.ListField(child=PhotosSerializer())
    videos = serializers.ListField(child=VideosSerializer())


class OrganizationOfficeHoursSerializer(serializers.Serializer):
    monday = serializers.CharField()
    tuesday = serializers.CharField()
    wednesday = serializers.CharField()
    thursday = serializers.CharField()
    friday = serializers.CharField()
    saturday = serializers.CharField()
    sunday = serializers.CharField()


class AdoptionSerializer(serializers.Serializer):
    policy = serializers.CharField()
    url = serializers.URLField()


# 200 Response Serializers
class AnimalCompleteSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    organization_id = serializers.CharField()
    url = serializers.URLField()
    type = serializers.CharField()
    species = serializers.CharField()
    breeds = BreedSerializer()
    colors = ColorSerializer()
    age = serializers.CharField()
    gender = serializers.CharField()
    size = serializers.CharField()
    coat = serializers.CharField()
    attributes = AttributeSerializer()
    environment = EnvironmentSerializer()
    tags = serializers.ListField(child=serializers.CharField())
    name = serializers.CharField()
    description = serializers.CharField()
    photos = serializers.ListField(child=PhotosSerializer())
    videos = serializers.ListField(child=VideosSerializer())
    status = serializers.CharField()
    published_at = serializers.DateTimeField()
    contact = ContactSerializer()
    distance = serializers.DecimalField(max_digits=5, decimal_places=4)


class AnimalListSerializer(serializers.Serializer):
    animals = serializers.ListField(child=AnimalSerializer())
    pagination = PaginationSerializer()


class AnimalTypeSerializer(serializers.Serializer):
    name = serializers.CharField()
    coats = serializers.ListField(child=serializers.CharField())
    colors = serializers.ListField(child=serializers.CharField())
    genders = serializers.ListField(child=serializers.CharField())


class AnimalBreedsSerializer(serializers.Serializer):
    name = serializers.CharField()


class OrganizationSerializer(serializers.Serializer):
    id = serializers.CharField()
    name = serializers.CharField()
    email = serializers.EmailField()
    phone = serializers.CharField()
    address = AddressSerializer()
    hours = OrganizationOfficeHoursSerializer()
    url = serializers.URLField()
    website = serializers.CharField()
    adoption = AdoptionSerializer()


# Query Serializers
class AnimalQuerySerializer(serializers.Serializer):
    type = serializers.CharField(required=False)
    page = serializers.IntegerField(required=False)
    location = serializers.CharField(required=False)
    gender = serializers.CharField(required=False)
    age = serializers.CharField(required=False)
    breed = serializers.CharField(required=False)