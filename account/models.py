from django.db import models
from django.db.models import SET_NULL, CASCADE
from django.contrib.auth.models import User
# Create your models here.


class Account(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=CASCADE,
        related_name='client_user',
        db_index=True
    )
    phone_number = models.CharField(max_length=50, null=True, blank=True)
    mobile_number = models.CharField(max_length=50, null=True, blank=True)
    address = models.TextField(max_length=256, null=True, blank=True)
    birthday = models.DateTimeField()

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    def __str__(self):
        return self.user.username