from django.contrib import admin
from account import models as account_model
# Register your models here.


class AccountAdmin(admin.ModelAdmin):
    list_display = ['id',  'get_name', 'get_email',]

    def get_email(self, obj):
        return "%s" % obj.user.email

    def get_name(self, obj):
        return "%s %s" % (obj.user.first_name, obj.user.last_name)

    get_name.short_description = 'Name'
    get_email.short_description = 'Email'


admin.site.register(account_model.Account, AccountAdmin)