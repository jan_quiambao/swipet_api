from rest_framework import serializers
from . import models as account_models
from datetime import datetime

class AccountSerializer(serializers.ModelSerializer):

    class Meta:
        model = account_models.Account
        fields = (
            'id',
            'full_name',
            'email',
            'username',
            'address',
            'phone_number',
            'mobile_number',
            'birthday',
        )

    id = serializers.SerializerMethodField("id_get")
    full_name = serializers.SerializerMethodField("full_name_get")
    email = serializers.SerializerMethodField("email_get")
    username = serializers.SerializerMethodField("username_get")
    birthday = serializers.SerializerMethodField("birthday_get")

    def id_get(self, obj):
        return obj.user.id

    def email_get(self, obj):
        return obj.user.email

    def birthday_get(self, obj):
        return datetime.strftime(obj.birthday, "%m-%d-%Y")

    def full_name_get(self, obj):
        return "%s %s" % (obj.user.first_name, obj.user.last_name)

    def username_get(self, obj):
        return obj.user.username