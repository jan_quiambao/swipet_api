from django.shortcuts import render
from config.base import BaseViewSet
from django.db import transaction
from rest_framework.decorators import action
from django.contrib.auth.models import User
from django.db.models import Q
from rest_framework.status import HTTP_200_OK
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from . import models as account_models, serializer as account_serializer
from utils.AccessTokenGenerator import *

# Create your views here.


class Account(BaseViewSet):

    @action(methods=["GET"], detail=False)
    def account_list(self, request, pk=None):

        account = account_models.Account.objects.all()

        return super().success_data_response(account_serializer.AccountSerializer(
            instance=account,
            many=True
        ).data)

    @action(methods=["GET"], detail=True)
    def get_account_info(self, request, pk=None):
        """
           <GET> /v1/account/<user_id>/get_account_info/
       """
        user = User.objects.filter(id=int(pk)).first()

        if not user:
            return super().error_response("User not found!")

        account = account_models.Account.objects.filter(
            user=user
        ).first()

        if not account:
            return super().error_response("User not found!")

        return super().success_data_response(account_serializer.AccountSerializer(
            instance=account,
            many=False
        ).data)

    @action(methods=["POST"], detail=False)
    def sign_up(self, request,):
        """
            <POST> /v1/account/sign_up/
            payload = {
                first_name: Reyan,
                last_name: Dela Cruz,
                email: reyan@directworksmedia.com,
                address: Miami Florida,
                phone_number: 123-423-42,
                mobile_number: 09123123123,
                birthday: jan 6,
            }
        """
        data = request.data
        exist_user = User.objects.filter(
            Q(email=data.get('email')) |
            Q(username=data.get('username'))
        ).first()

        if exist_user:
            return super().error_response("Email or Username is already exist")

        try:
            with transaction.atomic():
                user = User(
                    first_name=data.get('first_name'),
                    last_name=data.get('last_name'),
                    email=data.get('email'),
                    username=data.get('username')
                )
                user.save()

                account = account_models.Account(
                    user=user,
                    address=data.get('address'),
                    mobile_number=data.get('mobile_number'),
                    phone_number=data.get('phone_number'),
                    birthday=data.get('birthday'),
                )
                account.save()

            return super().success_response("Successfully save account!")
        except Exception as e:
            print(e)
            return super().error_response(e)

    @action(methods=["POST"], detail=False)
    def update_account(self, request):
        """
           <POST> /v1/account/sign_up/
           payload = {
                id: user_id,
               first_name: Reyan,
               last_name: Dela Cruz,
               email: reyan@directworksmedia.com,
               username:reyan
               address: Miami Florida,
               phone_number: 123-423-42,
               mobile_number: 09123123123,
               birthday: jan 6,
           }
       """
        data = request.data
        user = User.objects.filter(
            id=data.get('id')
        ).first()

        if not user:
            return super().error_response("User not found!")

        account = account_models.Account.objects.filter(
            user=user
        ).first()

        if not account:
            return super().error_response("Account not found!")

        try:
            with transaction.atomic():
                user.first_name = data.get('first_name')
                user.first_name = data.get('last_name')
                user.first_name = data.get('email')
                user.first_name = data.get('username')
                user.save()

                account.address = data.get('address')
                account.address = data.get('phone_number')
                account.address = data.get('mobile_number')
                account.address = data.get('birthday')

                account.save()

            return super().success_response("Successfully updated account!")
        except Exception as e:
            print(e)
            return super().error_response(e)

    @action(methods=["POST"], detail=False, permission_classes=[], authentication_classes=[])
    def login(self, request):
        """
            <POST> /v1/account/login/
            payload = {
                "client_id": L11UqhdB7s4QZYEVk2UQSBt5HdxNXCgYsvoOsLSu,
                "client_secret": CXV4YpBLlL4lC7AxtK6nvRhUgJgQYig1AsyaTf
                kFqLSvzvryFIrXn81IMNGsGEEImisGlqtqWrOH5CZDZrClWnKdEFR
                rsBhiDZ3wsMp0x9VflkjqdxYhUlZ1akRmPgQ7,
                "email": reyan@gmail.com,
                "password": User password
            }
        """
        data = request.data
        client_id = data.get("client_id")
        client_secret = data.get("client_secret")
        username = data.get("username")
        password = data.get("password")

        user = User.objects.filter(
            Q(username=username) |
            Q(email=username)
        ).first()

        if not user or not user.check_password(password):
            return super().error_response("Invalid username/password")

        account = account_models.Account.objects.filter(user=user).first()

        if not account:
            return super().error_response("Account not found!")

        try:

            refresh_token = AccessTokenGenerator.generate(request, user, client_id, client_secret)
            access_token = refresh_token.access_token

            response = {
                "success": True,
                "token": access_token.token,
                "user_id": user.id,
                "account_id": account.id,
                "full_name": "%s %s" % (user.first_name, user.last_name)
            }

            return Response(response, status=HTTP_200_OK)

        except Exception as e:
            print(e)
            return super().error_response(e)
